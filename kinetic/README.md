## Installation procedure of PX4 with ROS/Gazebo on Ubuntu 16.04
Please refer to [official wiki](https://dev.px4.io/v1.10/en/simulation/multi-vehicle-simulation.html) for more detailed explanations. However, please note that this documentation is for installation of PX4 1.9.0 on Ubuntu 16.04 _not_ Ubuntu 18.04. The following procedure outlines the necessary steps to install the firmware with PX4 on ROS/Gazebo:
1. Create a directory for firmware installation and change to that directory:
    ```
    ~$ mkdir ~/src
    ~$ cd ~/src
    ```
    _For example, the created directory is `src` in the home directory._
2. Install the development toolchains: 
    - Update the current repositories:
        ```
        ~$ sudo apt update
        ~$ sudo apt upgrade
        ```
    - Download `ubuntu.sh`, `requirements.txt` and `ubuntu_sim_ros_kinetic.sh` from this repository:
        ```
        ~$ wget https://gitlab.com/LebahDev/mod_px4/-/raw/master/kinetic/ubuntu.sh
        ~$ wget https://gitlab.com/LebahDev/mod_px4/-/raw/master/kinetic/requirements.txt
        ~$ wget https://gitlab.com/LebahDev/mod_px4/-/raw/master/kinetic/ubuntu_sim_ros_kinetic.sh
        ```        
    - Run the bash file with no arguments (in a bash shell) to install everything:
        ```
        ~$ source ubuntu.sh
        ```        
    - Run the following script to install:
        * Common dependencies libraries and tools as defined in `ubuntu_sim_common_deps.sh`
        * ROS Kinetic (including Gazebo7)
        * MAVROS       
        ```
        ~$ source ubuntu_sim_ros_kinetic.sh
        ```
3. Setup the PX4 firmware:
    - Download and change to the directory of the downloaded firmware:
        ```
        ~$ git clone https://github.com/PX4/Firmware.git -b release/1.9 --recursive
        ~$ cd Firmware
        ```
    - Compile the firmware:
        ```
        ~$ git submodule update --init --recursive
        ~$ DONT_RUN=1 make px4_sitl_default gazebo
        ```
    - Source the firmware environment and allow it to be executed automatically everytime a terminal is started:
        ```
        ~$ echo "source $(pwd)/Tools/setup_gazebo.bash $(pwd) $(pwd)/build/px4_sitl_default" >> ~/.bashrc
        ~$ echo "export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd):$(pwd)/Tools/sitl_gazebo" >> ~/.bashrc
        ```
4. Install the Ground Control System (GCS), in this case the QGroundControl is used:
    - Change to the directory for firmware installation:
        ```
        ~$ cd ~/src
        ```
    - Setup the environment and install the dependencies:
        ```
        ~$ sudo usermod -a -G dialout $USER
        ~$ sudo apt-get remove modemmanager -y
        ~$ sudo apt install gstreamer1.0-plugins-bad gstreamer1.0-libav -y
        ```
    - Download [QGroundControl](https://s3-us-west-2.amazonaws.com/qgroundcontrol/latest/QGroundControl.AppImage) and change the mode:
        ```
        ~$ wget https://s3-us-west-2.amazonaws.com/qgroundcontrol/latest/QGroundControl.AppImage
        ~$ chmod +x QGroundControl.AppImage
        ```
5. Close all terminals and reboot the computer.
6. Execute a multi-quadcopter simulation:   
    - Open the QGroundControl. This can be done either by double-clicking the file or typing the following command on a terminal:
        ```
        ~$ cd ~/src
        ~$ ./QGroundControl.AppImage
        ```
    - Open another terminal and run the following command:
        ```
        ~$ roslaunch px4 multi_uav_mavros_sitl.launch
        ```

**[Alternative]** step (1) to (5) can alternatively be executed as follows:
1. Download `setup_env_kinetic.sh` from this repository 
2. Execute `setup_env_kinetic.sh`:
    ```
    ~$ chmod +x setup_env_kinetic.sh
    ~$ ./setup_env_kinetic.sh
    ```