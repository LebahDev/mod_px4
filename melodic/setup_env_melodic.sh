#!/bin/bash

echo "[WARNING] This program will automatically restart your computer after it finishes."
read -p "Continue? [y/N]: " yn
case $yn in
[Nn]* ) exit;;
[Yy]* ) 

# Create a directory for firmware installation and change to that directory:    
echo "Make and change to the working directory"
mkdir ~/src
cd ~/src

# Update your current repositories
sudo apt update
sudo apt upgrade

# Download ubuntu.sh and requirements.txt from LebahDev repository:
echo "Setting up main toolchains"
wget https://gitlab.com/LebahDev/mod_px4/-/raw/master/melodic/ubuntu.sh
wget https://gitlab.com/LebahDev/mod_px4/-/raw/master/melodic/requirements.txt
wget https://gitlab.com/LebahDev/mod_px4/-/raw/master/melodic/ubuntu_sim_ros_melodic.sh
# Run the bash file with no arguments (in a bash shell) to install everything:
source ubuntu.sh
source ubuntu_sim_ros_melodic.sh

# Download and change to the directory of the downloaded firmware:
echo "Setting up the firmware"
cd ~/src
git clone https://github.com/PX4/Firmware.git --recursive
cd Firmware
git submodule update --init --recursive
DONT_RUN=1 make px4_sitl_default gazebo

# Source the downloaded firmware and allow it to be executed on startup:
echo "Sourcing the firmware and enabling it on startup"
echo "source $(pwd)/Tools/setup_gazebo.bash $(pwd) $(pwd)/build/px4_sitl_default" >> ~/.bashrc
echo "export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd):$(pwd)/Tools/sitl_gazebo" >> ~/.bashrc

# Download and setup QGroundControl
cd ~/src
sudo usermod -a -G dialout $USER
sudo apt-get remove modemmanager -y
sudo apt install gstreamer1.0-plugins-bad gstreamer1.0-libav -y
wget https://s3-us-west-2.amazonaws.com/qgroundcontrol/latest/QGroundControl.AppImage
chmod +x QGroundControl.AppImage

# Reboot computer
echo "Restarting computer"
sudo reboot;

esac